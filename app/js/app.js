
core.App = {
    init: function() {

        $(window).on('resize', core.App.onResize)
                 .on('scroll', core.App.onScroll);
    },

    onResize: function() {

    },
    onScroll: function() {
        modules.Header.onScroll();
    }
};
