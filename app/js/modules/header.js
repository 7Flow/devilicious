'use strict';

modules.Header = {
    threshold: 3,
    $el: $('header'),
    light: false,

    onScroll: function() {
        if ($(document).scrollTop() > modules.Header.threshold) {
            if (!modules.Header.light) {
                modules.Header.light = true;
                modules.Header.$el.addClass('light');
            }
        } else {
            if (modules.Header.light) {
                modules.Header.light = false;
                modules.Header.$el.removeClass('light');
            }
        }
    }
};
